#!/usr/bin/env bash
# Leave only one comment symbol on selected options
# Those with two commets will be ignored:
# The name to show in queue lists for this job:
#SBATCH -J train_semanticGAN_segmentation_modified.sh

# Number of desired cpus (can be in any node):
#SBATCH --ntasks=4

# Number of desired cpus (all in same node):
#SBATCH --cpus-per-task=8
#SBATCH --ntasks-per-node=4

# Amount of RAM needed for this job:
#SBATCH --mem=10gb

# The time the job will be running:
#SBATCH --time=05:00:00

# To use GPUs you have to request them:
#SBATCH --gres=gpu:1

##SBATCH --constraint=cal

# Set output and error files
#SBATCH --error=train_semanticGAN_segmentation_modified.%J.err
#SBATCH --output=train_semanticGAN_segmentation_modified.%J.out

# To load some software (you can show the list with 'module avail'):

module load miniconda/3
source activate /mnt/home/users/tic_163_uma/josdiafra/condaEnvironments/deepLearningCuda3.8

export OPENAI_LOGDIR="/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified"
export PYTHONPATH="/mnt/home/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified"
export CUDA_VISIBLE_DEVICES=0,1
export PYTORCH_CUDA_ALLOC_CONF=max_split_size_mb:32

# the program to execute with its parameters:
##echo time python curso_ok.py --epochs 3 --batch-size 512
echo "Init job (SemanticGAN): train segmentation modified"


#time /mnt/home/users/tic_163_uma/josdiafra/condaEnvironments/deepLearningCuda3.8/bin/python3.8 -m torch.distributed.launch --nproc_per_node=2 /mnt/home/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/semanticGAN/train_segModified.py --iter 2000

time /mnt/home/users/tic_163_uma/josdiafra/condaEnvironments/deepLearningCuda3.8/bin/python3.8 /mnt/home/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/semanticGAN/train_segModified.py --iter 10000
#time /mnt/home/users/tic_163_uma/josdiafra/condaEnvironments/deepLearningCuda3.8/bin/python3.8 /mnt/home/users/tic_163_uma/josdiafra/PhD/semanticGAN/semanticGAN/train_seg_gan.py --img_dataset /mnt/scratch/users/tic_163_uma/josdiafra/datasets/celebA-HQ --seg_dataset /mnt/scratch/users/tic_163_uma/josdiafra/datasets/celebA-HQ --inception /mnt/home/users/tic_163_uma/josdiafra/PhD/semanticGAN/results/inception/inception_results_512.pkl --seg_name celeba-mask --checkpoint_dir /mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN/segmentation --iter 100000

