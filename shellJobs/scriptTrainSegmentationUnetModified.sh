#!/usr/bin/env bash
# Leave only one comment symbol on selected options
# Those with two commets will be ignored:
# The name to show in queue lists for this job:
#SBATCH -J train_semanticGAN_segmentation_modified_unet_ISIC.sh

# Number of desired cpus (can be in any node):
#SBATCH --ntasks=4

# Number of desired cpus (all in same node):
#SBATCH --cpus-per-task=8
#SBATCH --ntasks-per-node=4

# Amount of RAM needed for this job:
#SBATCH --mem=40gb

# The time the job will be running:
#SBATCH --time=96:00:00

# To use GPUs you have to request them:
#SBATCH --gres=gpu:1
#SBATCH --constraint=dgx

##SBATCH --constraint=cal

# Set output and error files
#SBATCH --error=train_semanticGAN_segmentation_modified_unet_CelebA.%J.err
#SBATCH --output=train_semanticGAN_segmentation_modified_unet_CelebA.%J.out

# To load some software (you can show the list with 'module avail'):

##module load miniconda/3
##source activate /mnt/scratch/users/tic_163_uma/josdiafra/condaEnvironments/deepLearning3.8
module load pytorch
module load singularity


export OPENAI_LOGDIR="/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified-unet"
export PYTHONPATH="/mnt/home/users/tic_163_uma/josdiafra/PhD/diffusion-model-modified"
export CUDA_VISIBLE_DEVICES=0 #,1
export PYTORCH_CUDA_ALLOC_CONF=max_split_size_mb:32
export CUDA_HOME=/usr/local/cuda

# the program to execute with its parameters:
echo "Init job (SemanticGAN): train segmentation unet modified CelebA"
nvidia-smi


#time /mnt/scratch/users/tic_163_uma/josdiafra/condaEnvironments/deepLearning3.8/bin/python3.8 /mnt/home/users/tic_163_uma/josdiafra/PhD/diffusion-model-modified/scripts/image_segmentation_draft.py
#time python /mnt/home/users/tic_163_uma/josdiafra/PhD/diffusion-model-modified/scripts/image_segmentation_draft.py
time python /mnt/home/users/tic_163_uma/josdiafra/PhD/diffusion-model-modified/scripts/image_segmentation_draft_CelebA_v2.py