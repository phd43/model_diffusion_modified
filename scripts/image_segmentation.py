import torch as th
from torch import nn
from torch.nn import functional as F
import torch.nn.utils.spectral_norm as spectral_norm
import numpy as np
import torchvision.transforms as T
from torch.utils import data
from torch import optim
from torch import distributed as dist
from torchvision.utils import save_image

from PIL import Image
import argparse

import os
import math
import time

from guided_diffusion.segmentation import GeneratorSeg
from guided_diffusion.losses import SoftmaxLoss, DiceLoss
from guided_diffusion import dist_util, logger


import sys
sys.path.append('/mnt/home/users/tic_163_uma/josdiafra/PhD/diffusion-model-modified/guided_diffusion/modelsSemantic')

from guided_diffusion.modelsSemantic.utils import *
from guided_diffusion.dataloader.dataset import CelebAMaskDataset
from guided_diffusion.utils.distributed import (
    get_rank,
    synchronize,
    reduce_loss_dict,
)

timestr = time.strftime("%Y%m%d-%H%M%S")

def mask2rgb(seg_name, mask):
    if seg_name == 'celeba-mask':
        color_table = th.tensor(
            [[0, 0, 0],
             [0, 0, 205],
             [132, 112, 255],
             [25, 25, 112],
             [187, 255, 255],
             [102, 205, 170],
             [227, 207, 87],
             [142, 142, 56]], dtype=th.float)
    else:
        raise Exception('No such a dataloader!')

    rgb_tensor = F.embedding(mask, color_table).permute(0, 3, 1, 2)
    return rgb_tensor

def batch_overlay(seg_dim, img_tensor, mask_tensor, alpha=0.3):
    b = img_tensor.shape[0] #3
    overlays = []
    imgs_np = make_image(img_tensor)
    if seg_dim == 1:
        idx = np.nonzero(mask_tensor.detach().cpu().numpy()[:, 0, :, :])
        masks_np = np.zeros((mask_tensor.shape[0], mask_tensor.shape[2], mask_tensor.shape[3], 3), dtype=np.uint8)
        masks_np[idx] = (0, 255, 0)
    else:
        masks_np = mask_tensor.detach().cpu().permute(0, 2, 3, 1).type(th.uint8).numpy()

    for i in range(b):
        img_pil = Image.fromarray(imgs_np[i]).convert('RGBA')
        mask_pil = Image.fromarray(masks_np[i]).convert('RGBA')

        overlay_pil = Image.blend(img_pil, mask_pil, alpha)
        overlay_tensor = T.functional.to_tensor(overlay_pil)
        overlays.append(overlay_tensor)
    overlays = th.stack(overlays, dim=0)
    return overlays

def make_image(tensor):
    return (
        tensor.detach()
            .clamp_(min=-1, max=1)
            .add(1)
            .div_(2)
            .mul(255)
            .type(th.uint8)
            .to('cpu')
            .numpy()
    )
def make_mask(seg_dim, tensor, threshold=0.5):
    if seg_dim == 1:
        seg_prob = th.sigmoid(tensor)
        seg_mask = th.zeros_like(tensor)
        seg_mask[seg_prob > threshold] = 1.0
        seg_mask = (seg_mask.to('cpu')
                       .mul(255)
                       .type(th.uint8)
                       .permute(0, 2, 3, 1)
                       .numpy())
    else:
        seg_prob = th.argmax(tensor, dim=1)
        seg_mask = mask2rgb('celeba-mask', seg_prob)
        seg_mask = (seg_mask.to('cpu')
                       .type(th.uint8)
                       .permute(0, 2, 3, 1)
                       .numpy())
    

    return seg_mask

def get_transformation(args):
    if args.seg_name == 'celeba-mask':
        transform = T.Compose(
            [
                T.ToTensor(),
                T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5), inplace=True)
            ]
        )
    else:
        raise Exception('No such a dataloader!')

    return transform

def data_sampler(dataset, shuffle, distributed):
    if distributed:
        return data.distributed.DistributedSampler(dataset, shuffle=shuffle)

    if shuffle:
        return data.RandomSampler(dataset)

    else:
        return data.SequentialSampler(dataset)

def sample_data(loader):
    while True:
        for batch in loader:
            yield batch

""" Gradient averaging. """
def average_gradients(model):
    size = float(dist.get_world_size())
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.ReduceOp.SUM)
        param.grad.data /= size


def requires_grad(model, flag=True):
    for p in model.parameters():
        p.requires_grad = flag


def train(args, pbar, segGen, img_loader, g_optim, device):

    ce_loss_func = SoftmaxLoss(tau=0.1)
    dice_loss_func = DiceLoss(sigmoid_tau=0.3)

    start_iter = 0
    seg_loader = sample_data(img_loader)

    for idx in pbar:
        #th.cuda.empty_cache()
        seg_data = next(seg_loader)
        i = idx + start_iter

        if i > args.iter:
            print('Done!')
            break

        for j in range(0,img_loader.batch_size):
            imageTensor = seg_data['image_intermediate_layer'][j].unsqueeze(0).to(device)
            maskTensor = seg_data['mask'][j].unsqueeze(0).to(device)

            #channelPooling = ChannelPooling(inChannels=imageTensor.size()[1], outChannels=1, activation=False)
            #imageTensor_channelPooling = channelPooling(imageTensor)

            fake_seg = segGen(imageTensor)
            #fake_seg_mean = th.mean(fake_seg, dim=0).type(th.float).unsqueeze(0)

            seg_mask_ce = th.argmax(maskTensor, dim=1)
            seg_mask_dice = (maskTensor + 1.0) / 2.0  
            g_label_ce_loss = ce_loss_func(fake_seg, seg_mask_ce)
            g_label_dice_loss = dice_loss_func(fake_seg, seg_mask_dice)
            
            g_label_loss = (g_label_ce_loss * args.lambda_label_ce + g_label_dice_loss * args.lambda_label_dice)

            segGen.zero_grad()
            g_label_loss.backward()
            #average_gradients(segGen)
            g_optim.step()

            #if get_rank() == 0:
            print(f'Iter:{i}, Loss image {j}: {g_label_loss}')

        if idx % 10 == 0:
            if get_rank() == 0 & args.distributed:
                print(f'Saved model, epoch: {idx}')
                path_save = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/model_saved_{timestr}.pt'
                th.save(segGen.state_dict(), path_save)
            else:
                print(f'Saved model, epoch: {idx}')
                path_save = f'/mnt/scratch/users/tic_163_uma/josdiafra/PhD/semanticGAN-modified/segmentation-modified/model_saved_{timestr}.pt'
                th.save(segGen.state_dict(), path_save)

        if args.distributed:
            th.distributed.destroy_process_group()
    
    print('---End Trainning---')

def train_v2(args, pbar, segGen, intermediateLayersArray, masksArray, g_optim, device):

    ce_loss_func = SoftmaxLoss(tau=0.1)
    dice_loss_func = DiceLoss(sigmoid_tau=0.3)

    start_iter = 0
    
    for idx in range(0,len(intermediateLayersArray)):
        
        i = idx + start_iter

        if i > args.iter:
            print('Done!')
            break
        
        layersTensor = intermediateLayersArray[i].unsqueeze(0).to(device)
        maskTensor = masksArray[i].unsqueeze(0).to(device)

        fake_seg = segGen(layersTensor)
        
        seg_mask_ce = th.argmax(maskTensor, dim=1)
        seg_mask_dice = (maskTensor + 1.0) / 2.0  

        g_label_ce_loss = ce_loss_func(fake_seg, seg_mask_ce)
        g_label_dice_loss = dice_loss_func(fake_seg, seg_mask_dice)
        
        g_label_loss = (g_label_ce_loss * args.lambda_label_ce + g_label_dice_loss * args.lambda_label_dice)

        segGen.zero_grad()
        g_label_loss.backward()
        #average_gradients(segGen)
        g_optim.step()

        #if get_rank() == 0:
        logger.log(f'Image:{i}, Loss image: {g_label_loss}')


if __name__ == '__main__':
    device = 'cpu' if th.cuda.is_available() != True else 'cuda'

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--img_dataset', type=str)
    parser.add_argument('--seg_dataset', type=str)
    parser.add_argument('--enc_backbone', type=str, default='fpn')
    parser.add_argument('--seg_name', type=str, default='celeba-mask')
    parser.add_argument('--batch', type=int, default=4)
    parser.add_argument('--n_gpu', type=int, default=0)
    parser.add_argument('--iter', type=int, default=1000)
    parser.add_argument('--lambda_label_ce', type=float, default=1.0)
    parser.add_argument('--lambda_label_dice', type=float, default=1.0)
    parser.add_argument('--g_reg_every', type=int, default=4)
    parser.add_argument('--lr', type=float, default=0.002)
    parser.add_argument('--local_rank', type=int, default=0)

    args = parser.parse_args()

    pbar = range(args.iter)
    img_dataset_path = '/mnt/scratch/users/tic_163_uma/josdiafra/datasets/celebA-HQ-256'
    transform = get_transformation(args)
    limit_size = None
    size = 256

    n_gpu = int(os.environ['WORLD_SIZE']) if 'WORLD_SIZE' in os.environ else 1

    print(f'Number gpu(s): {n_gpu}')

    args.n_gpu = n_gpu
    args.distributed = n_gpu > 1

    if args.distributed:
        th.cuda.set_device(args.local_rank)
        th.distributed.init_process_group(backend='nccl', init_method='env://')
        synchronize()

    img_dataset = CelebAMaskDataset(args, img_dataset_path, unlabel_transform=transform,
                                            limit_size=limit_size,
                                            is_label=True, phase='train',resolution=size)

    img_loader = data.DataLoader(
            img_dataset,
            batch_size=args.batch,
            sampler=data_sampler(img_dataset, shuffle=False, distributed=args.distributed),
            drop_last=True,
            pin_memory=True,
            num_workers=4,
        )

    #img = next(iter(img_loader))

    #seg_loader = sample_data(img_loader)
    #seg_data = next(seg_loader)

    channels = 256
    seg_dim = 8
    style_dim = 256
    size = 256
    n_mlp = 8
    channel_multiplier = 2
    inChannels = 103
    outChannels = 1
    g_reg_ratio = args.g_reg_every / (args.g_reg_every + 1)

    segGen = GeneratorSeg(size,style_dim,n_mlp,inChannels,outChannels,False,seg_dim=seg_dim,channel_multiplier=channel_multiplier).to(device)

    if args.distributed:
        segGen = nn.parallel.DistributedDataParallel(
            segGen,
            device_ids=[args.local_rank],
            output_device=args.local_rank,
            broadcast_buffers=False,
            find_unused_parameters=True,
        )

    g_optim = optim.Adam(
            segGen.parameters(),
            lr=args.lr * g_reg_ratio,
            betas=(0 ** g_reg_ratio, 0.99 ** g_reg_ratio),
        )

    requires_grad(segGen, True)         

    train(args, pbar, segGen,img_loader, g_optim, device)