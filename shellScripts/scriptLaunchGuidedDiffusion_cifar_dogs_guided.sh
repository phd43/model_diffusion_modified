#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffusion/samples/cifar_dogs"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

python /home/joseangel/Documents/guided-diffusion-main/scripts/classifier_sample.py  --model_path /home/joseangel/Documents/guided-diffusion-main/models/cifar_dogs/model040000_cifar_dogs_birds_model_guided.pt --classifier_path /home/joseangel/Documents/guided-diffusion-main/models/cifar_dogs/model299999_cifar_birds_dogs_clasiffier.pt --attention_resolutions 32,16,8 --class_cond True --image_size 64 --learn_sigma True --num_channels 64 --num_heads 4 --num_res_blocks 2 --resblock_updown True --use_fp16 False --use_scale_shift_norm True --image_size 64 --classifier_attention_resolutions 32,16,8 --classifier_depth 2 --classifier_width 64 --classifier_pool attention --classifier_resblock_updown True --classifier_use_scale_shift_norm True --classifier_scale 1.0 --classifier_use_fp16 False --batch_size 4 --num_samples 10000 --timestep_respacing 1000 --use_ddim False

echo "PID of this script: $$"
