#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffusion/modelSR"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

#python ./scripts/super_res_train.py --data_dir /home/joseangel/Documents/datasets/datasets/cifar/cifar_train_dogs --lr_anneal_steps 40000 --batch_size 2
#python ./scripts/super_res_train.py --data_dir /home/joseangel/Documents/datasets/datasets/cifar/cifar_train_dogs --num_res_blocks 3 --learn_sigma True --class_cond False --diffusion_steps 1000 --noise_schedule linear --use_kl True --lr 1e-4 --batch_size 2 --lr_anneal_steps 40000
python ./scripts/super_res_train.py --data_dir /home/joseangel/Documents/datasets/datasets/cifar/cifar_train_dogs --num_res_blocks 3 --learn_sigma True --class_cond False --diffusion_steps 1000 --noise_schedule cosine --use_kl True --lr 1e-4 --batch_size 2 --lr_anneal_steps 40000 --schedule_sampler loss-second-moment --weight_decay 0.05

echo "PID of this script: $$"
