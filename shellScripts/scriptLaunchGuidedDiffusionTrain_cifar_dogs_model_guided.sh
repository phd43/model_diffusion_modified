#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffusion/models"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

python /home/joseangel/Documents/guided-diffusion-main/scripts/image_train.py --data_dir /home/joseangel/Documents/datasets/datasets/cifar_64/train/images_labeled --lr_anneal_steps 40000 --batch_size 16 --lr 3e-4 --weight_decay 0.05  --image_size 64 --attention_resolutions 32,16,8 --class_cond True --learn_sigma True --num_channels 64 --num_heads 4 --num_res_blocks 2 --resblock_updown True --use_fp16 False --use_scale_shift_norm True


echo "PID of this script: $$"
