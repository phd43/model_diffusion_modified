#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffursion-LSUN/models"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

python /home/joseangel/Documents/guided-diffusion-main/scripts/image_train.py --data_dir /home/joseangel/Documents/datasets/datasets/cifar_64/train/dogs --image_size 64 --num_channels 64 --num_res_blocks 3 --learn_sigma True --class_cond True --diffusion_steps 4000 --noise_schedule cosine --use_kl True --lr 1e-4 --batch_size 128 --schedule_sampler loss-second-moment --lr_anneal_steps 20000 --microbatch 16

echo "PID of this script: $$"
