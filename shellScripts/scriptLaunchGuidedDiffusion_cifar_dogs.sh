#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffusion/samples/cifar_dogs"
export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

python /home/joseangel/Documents/guided-diffusion-main/scripts/image_sample.py --image_size 64 --attention_resolutions 32,16,8 --class_cond False --learn_sigma True --num_channels 64 --num_heads 4 --num_res_blocks 2 --resblock_updown True --use_fp16 False --use_scale_shift_norm True --use_kl True --noise_schedule cosine  --model_path /home/joseangel/Documents/guided-diffusion-main/models/cifar_dogs/model040000_cifar_dogs_cosine_kl.pt

echo "PID of this script: $$"
