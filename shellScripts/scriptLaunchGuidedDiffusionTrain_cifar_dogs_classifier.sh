#!/bin/sh

export OPENAI_LOGDIR="/home/joseangel/Desktop/guided-diffusion/models"
    export PYTHONPATH="/home/joseangel/Documents/guided-diffusion-main"

python /home/joseangel/Documents/guided-diffusion-main/scripts/classifier_train.py --data_dir /home/joseangel/Documents/datasets/datasets/cifar_64/train/images_labeled --iterations 1000 --learn_sigma True --anneal_lr True --batch_size 2 --lr 3e-4 --save_interval 10000 --weight_decay 0.05 --image_size 64 --classifier_attention_resolutions 32,16,8 --classifier_depth 2 --classifier_width 64 --classifier_pool attention --classifier_resblock_updown True --classifier_use_scale_shift_norm True

echo "PID of this script: $$"
